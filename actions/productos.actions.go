package actions

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.com/skolldevs-backend/sanus-api/authentication"
	"gitlab.com/skolldevs-backend/sanus-api/database"
	"gitlab.com/skolldevs-backend/sanus-api/models"
	"gitlab.com/skolldevs-backend/sanus-api/utils"
	"gopkg.in/mgo.v2/bson"
	"net/http"
)

func CreateProducto(w http.ResponseWriter, r *http.Request) {

	isAuth := authentication.ValidateToken(r)
	if (isAuth != "") {
		utils.RespondWithError(w, http.StatusUnauthorized, isAuth)
		return
	}

	isAdmin := authentication.IsUserAdmin(r);
	if isAdmin == false {
		utils.RespondWithError(w, http.StatusUnauthorized, "No es administrador")
		return
	}

	defer r.Body.Close()
	var producto models.Producto
	if err := json.NewDecoder(r.Body).Decode(&producto); err != nil {
		utils.RespondWithError(w, http.StatusInternalServerError, "Error al decodificar parametros de entrada.")
		return
	}

	producto.ID = bson.NewObjectId()
	producto.Create_date, _ = utils.GetDateTimeActual()

	if err := database.GetCollection(database.PRODUCTOS).Insert(producto); err != nil {
		utils.RespondWithError(w, http.StatusInternalServerError, "Error al crear producto.")
		return
	}

	utils.RespondWithJSON(w, http.StatusCreated, producto)
}

func GetAllProductos(w http.ResponseWriter, r *http.Request) {

	var prductos []models.Producto
	err := database.GetCollection(database.PRODUCTOS).Find(nil).Sort("+_id").All(&prductos)
	if err != nil {
		utils.RespondWithError(w, http.StatusInternalServerError, "No se encontraron registros en la coleccion.")
		return
	}
	utils.RespondWithJSON(w, http.StatusOK, prductos)
}

func GetProductosByCategoria(w http.ResponseWriter, r *http.Request) {

	params := mux.Vars(r)
	category := params["categoria"]
	var productos []models.Producto

	err := database.GetCollection(database.PRODUCTOS).Find(bson.M{"categoria": category, "estado": "Activo"}).All(&productos)
	if (err != nil) {
		utils.RespondWithError(w, http.StatusNotFound, "No se encontraron registros.")
		return
	}
	utils.RespondWithJSON(w, http.StatusOK, productos)
}

func UpdateProducto(w http.ResponseWriter, r *http.Request) {

	isAuth := authentication.ValidateToken(r)
	if (isAuth != "") {
		utils.RespondWithError(w, http.StatusUnauthorized, isAuth)
		return
	}

	isAdmin := authentication.IsUserAdmin(r);
	if isAdmin == false {
		utils.RespondWithError(w, http.StatusUnauthorized, "No es administrador")
		return
	}

	params := mux.Vars(r)
	productoID := params["id"]

	if !bson.IsObjectIdHex(productoID) {
		utils.RespondWithError(w, http.StatusBadRequest, "El id no es un ObjectIdHex valido.")
		return
	}
	decoder := json.NewDecoder(r.Body)
	var producto_data models.Producto

	err := decoder.Decode(&producto_data)
	if (err != nil) {
		utils.RespondWithError(w, http.StatusInternalServerError, "Error en el envio de parametros entrada de Producto.")
		return
	}
	defer r.Body.Close()

	idDeserialize := bson.ObjectIdHex(productoID)

	document := bson.M{"_id": idDeserialize}
	change := bson.M{"$set": producto_data}

	error := database.GetCollection(database.PRODUCTOS).Update(document, change)

	if (error != nil) {
		fmt.Println(error)
		utils.RespondWithError(w, http.StatusInternalServerError, "Error al actualizar producto.")
		return
	}
	utils.RespondWithJSON(w, http.StatusOK, producto_data)
}

func FindProductosByWords(w http.ResponseWriter, r *http.Request) {

	params := mux.Vars(r)
	texto := params["texto"]
	var productos []models.Producto

	err := database.GetCollection(database.PRODUCTOS).
		Find(bson.M{"nombre": bson.M{"$regex": bson.RegEx{texto, "i"}},
		}).Sort("+create_date").All(&productos)

	if err != nil {
		utils.RespondWithError(w, http.StatusInternalServerError, "No se encontraron registros en la coleccion.")
		return
	}
	utils.RespondWithJSON(w, http.StatusOK, productos)
}

func GetProductosDestacados(w http.ResponseWriter, r *http.Request) {

	var productos []models.Producto

	params := bson.M{"destacado": true, "estado": "Activo"}

	err := database.GetCollection(database.PRODUCTOS).Find(params).Sort("+_id").All(&productos)
	if err != nil {
		utils.RespondWithError(w, http.StatusInternalServerError, "No se encontraron registros en la coleccion.")
		return
	}
	utils.RespondWithJSON(w, http.StatusOK, productos)
}

func GetConteoProductos(w http.ResponseWriter, r *http.Request) {

	f1:="2019-08-01T00:00:00.000Z"
	f2:="2019-08-31T23:59:59.000Z"

	fecha1,fecha2,_ := utils.SetFechaTimeToZero(f1,f2)

	fmt.Println(fecha1)
	fmt.Println(fecha2)

	var items models.ConteoProd
	var arr []models.ConteoProd

	var productos []models.Producto
	err := database.GetCollection(database.PRODUCTOS).Find(nil).Sort("+_id").All(&productos)
	if err != nil {
		utils.RespondWithError(w, http.StatusInternalServerError, "No se encontraron registros en la coleccion.")
		return
	}

	for _, value := range productos {

		paramsP := bson.M{"productos._id": value.ID, "fecha_entrega2": bson.M{"$gt": fecha1, "$lt": fecha2}}
		cant,errP := database.GetCollection(database.PEDIDOS).Find(paramsP).Count()
		if errP != nil {
			utils.RespondWithError(w, http.StatusInternalServerError, "Error al obetener pedidos de perdidas.")
			return
		}
		items.Nombre = value.Nombre
		items.Cantidad = cant
		arr = append(arr, items)

	}

	utils.RespondWithJSON(w, http.StatusOK, arr)

}
