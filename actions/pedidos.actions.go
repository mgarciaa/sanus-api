package actions

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"gitlab.com/skolldevs-backend/sanus-api/authentication"
	"gitlab.com/skolldevs-backend/sanus-api/database"
	"gitlab.com/skolldevs-backend/sanus-api/models"
	"gitlab.com/skolldevs-backend/sanus-api/utils"
	"gopkg.in/mgo.v2/bson"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"
)

func GetAllPedidos(w http.ResponseWriter, r *http.Request) {

	isAuth := authentication.ValidateToken(r)
	if (isAuth != "") {
		utils.RespondWithError(w, http.StatusUnauthorized, isAuth)
		return
	}

	var pedidos []models.Pedido

	err := database.GetCollection(database.PEDIDOS).Find(nil).Sort("+numero").All(&pedidos)
	if err != nil {
		utils.RespondWithError(w, http.StatusInternalServerError, "No se encontraron registros en la coleccion.")
		return
	}
	utils.RespondWithJSON(w, http.StatusOK, pedidos)
}

func CreatePedido(w http.ResponseWriter, r *http.Request) {

	isAuth := authentication.ValidateToken(r)
	if (isAuth != "") {
		utils.RespondWithError(w, http.StatusUnauthorized, isAuth)
		return
	}

	isAdmin := authentication.IsUserAdmin(r);
	if isAdmin == false {
		utils.RespondWithError(w, http.StatusUnauthorized, "No tiene permisos para realizar esta accion")
		return
	}

	defer r.Body.Close()
	var pedido models.Pedido
	if err := json.NewDecoder(r.Body).Decode(&pedido); err != nil {
		utils.RespondWithError(w, http.StatusInternalServerError, "Error al leer el body.")
		return
	}
	pedido.FechaCreacion, _ = utils.GetDateTimeActual()
	pedido.FechaEntrega2, _ = utils.ParseFecha(pedido.FechaEntrega)

	numero, _ := database.CreateSecuence(database.PEDIDOS)
	pedido.Numero = numero

	if err := database.GetCollection(database.PEDIDOS).Insert(pedido); err != nil {
		utils.RespondWithError(w, http.StatusBadRequest, "No se pudo insertar el resgistro.")
		return
	}

	utils.RespondWithJSON(w, http.StatusCreated, pedido)
}

func FindPedidoByID(w http.ResponseWriter, r *http.Request) {

	isAuth := authentication.ValidateToken(r)
	if (isAuth != "") {
		utils.RespondWithError(w, http.StatusUnauthorized, isAuth)
		return
	}

	params := mux.Vars(r)
	idPedido := params["id"]

	if !bson.IsObjectIdHex(idPedido) {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	var resultado models.Pedido

	err := database.GetCollection(database.PEDIDOS).FindId(bson.ObjectIdHex(idPedido)).One(&resultado)
	if (err != nil) {
		utils.RespondWithError(w, http.StatusNotFound, "No se encontró el registro.")
		return
	}
	utils.RespondWithJSON(w, http.StatusOK, resultado)
}

func UpdatePedido(w http.ResponseWriter, r *http.Request) {

	isAuth := authentication.ValidateToken(r)
	if (isAuth != "") {
		utils.RespondWithError(w, http.StatusUnauthorized, isAuth)
		return
	}

	isAdmin := authentication.IsUserAdmin(r);
	if isAdmin == false {
		utils.RespondWithError(w, http.StatusUnauthorized, "No tiene permisos para realizar esta accion")
		return
	}

	params := mux.Vars(r)
	pedidoId := params["id"]

	if !bson.IsObjectIdHex(pedidoId) {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	decoder := json.NewDecoder(r.Body)
	var pedido_data models.Pedido

	err := decoder.Decode(&pedido_data)
	if (err != nil) {
		utils.RespondWithError(w, http.StatusInternalServerError, "Error al enviar json de pedido.")
		return
	}
	defer r.Body.Close()

	pedido_data.FechaEntrega2, _ = utils.ParseFecha(pedido_data.FechaEntrega)

	idDeserialize := bson.ObjectIdHex(pedidoId);
	document := bson.M{"_id": idDeserialize}
	change := bson.M{"$set": pedido_data}

	error := database.GetCollection(database.PEDIDOS).Update(document, change)

	if (error != nil) {
		log.Fatal(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Header().Set("Access-Control-Allow-Origin", "*")

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(pedido_data)
}

func GetPedidosByEstado(w http.ResponseWriter, r *http.Request) {

	isAuth := authentication.ValidateToken(r)
	if (isAuth != "") {
		utils.RespondWithError(w, http.StatusUnauthorized, isAuth)
		return
	}

	isAdmin := authentication.IsUserAdmin(r);
	if isAdmin == true {
		PedidosAdminRol(w, r)
	} else {
		userEmail := authentication.GetUserEmailFromClaims(r)
		PedidosUserRol(w, r, userEmail)
	}

}

func PedidosAdminRol(w http.ResponseWriter, r *http.Request) {

	q := r.URL.Query()
	estado := q.Get("estado")
	usuario := q.Get("usuario")
	pagina, _ := strconv.Atoi(q.Get("pag"))
	limit, _ := strconv.Atoi(q.Get("limit"))

	var pedidos []models.Pedido
	var parametros bson.M

	if (usuario != "") {
		parametros = bson.M{"estado": estado, "usuario_designado.email": usuario, "ciudad": "Santiago"}
	} else {
		parametros = bson.M{"estado": estado, "ciudad": "Santiago"}
	}

	query := database.GetCollection(database.PEDIDOS).Find(parametros).Sort("+fecha_entrega").Limit(limit).Skip((pagina - 1) * limit).All(&pedidos)

	if (query != nil) {
		utils.RespondWithError(w, http.StatusInternalServerError, "Error al paginar pedidos")
		log.Fatal(query)
	}

	cantidadReg, err := database.GetCollection(database.PEDIDOS).Find(parametros).Count()
	if (err != nil) {
		utils.RespondWithError(w, http.StatusInternalServerError, "Error al contar total registros")
		return
	}

	var pedidoT models.PedidoT
	pedidoT.Pedidos = pedidos
	pedidoT.TotalRows = cantidadReg

	utils.RespondWithJSON(w, http.StatusOK, pedidoT)

}

func PedidosUserRol(w http.ResponseWriter, r *http.Request, userEmail string) {

	q := r.URL.Query()
	estado := q.Get("estado")
	pagina, _ := strconv.Atoi(q.Get("pag"))
	limit, _ := strconv.Atoi(q.Get("limit"))

	var pedidos []models.Pedido

	parametros := bson.M{"usuario_designado.email": userEmail, "estado": estado}
	query := database.GetCollection(database.PEDIDOS).Find(parametros).Sort("+fecha_entrega").Limit(limit).Skip((pagina - 1) * limit).All(&pedidos)

	if (query != nil) {
		utils.RespondWithError(w, http.StatusInternalServerError, "Error al paginar pedidos")
		log.Fatal(query)
	}

	cantidadReg, err := database.GetCollection(database.PEDIDOS).Find(parametros).Count()
	if (err != nil) {
		utils.RespondWithError(w, http.StatusInternalServerError, "Error al contar total registros")
		return
	}

	var pedidoT models.PedidoT
	pedidoT.Pedidos = pedidos
	pedidoT.TotalRows = cantidadReg

	utils.RespondWithJSON(w, http.StatusOK, pedidoT)

}

func GetFechas(w http.ResponseWriter, r *http.Request) {

	var fechas models.Fechas
	var FechaInicio, FechaFin, fechaActual = utils.GetFechaActual()
	fechas.FechaInicio = FechaInicio
	fechas.FechaFin = FechaFin
	fechas.FechaActual = fechaActual
	utils.RespondWithJSON(w, http.StatusOK, fechas)
}

func obtenerGastos(f time.Time, fecha2 time.Time, w http.ResponseWriter) int {

	var total_gastos = 0

	dateString := fecha2.String()
	dateArr := strings.Split(dateString, "-")

	anio := dateArr[0]
	mes := dateArr[1]

	fechaParsed, _ := utils.ParseFecha(anio + "-" + mes + "-01 00:00:00")

	var gastos []models.MesGasto
	params := bson.M{"fecha_mensual": fechaParsed}
	err := database.GetCollection(database.GASTOS).Find(params).All(&gastos)
	if err != nil {
		utils.RespondWithError(w, http.StatusInternalServerError, "Error al obetener pedidos entregados.")

	}

	if (len(gastos) > 0) {
		// Suma total de lo vendido y Suma total del costo de lo vendido para gastos
		for _, value := range gastos[0].GastosMensuales {
			total_gastos += value.Precio_total
		}
	}

	return total_gastos
}

func GetFinanzasByFechaAndUser(w http.ResponseWriter, r *http.Request) {

	isAuth := authentication.ValidateToken(r)
	if (isAuth != "") {
		utils.RespondWithError(w, http.StatusUnauthorized, isAuth)
		return
	}

	var response models.FinanzasResponse
	total_ventas := 0
	total_costos := 0
	total_gastos := 0
	total_costo_envio := 0
	utilidad := 0
	utilidad_30 := 0
	utilidad_50 := 0
	utilidad_70 := 0
	total_ganacia_sanus := 0

	total_pedidos_perdidas := 0
	total_costo_pedidos_perdidas := 0

	defer r.Body.Close()
	var finanzasReq models.FinanzasRequest
	if err := json.NewDecoder(r.Body).Decode(&finanzasReq); err != nil {
		utils.RespondWithError(w, http.StatusInternalServerError, "Error al leer el body.")
		return
	}

	usuario := finanzasReq.UsuarioEmail;
	fecha1, fecha2, _ := utils.SetFechaTimeToZero(finanzasReq.Fecha_inicio, finanzasReq.Fecha_fin)

	// obteniendo cantidad de pedidos perdidas
	var pedidosPerdidas models.Pedidos
	paramsP := bson.M{"estado": "perdida", "usuario_designado.email": usuario, "fecha_entrega2": bson.M{"$gt": fecha1, "$lt": fecha2}}
	errP := database.GetCollection(database.PEDIDOS).Find(paramsP).Sort("+numero").All(&pedidosPerdidas)
	if errP != nil {
		utils.RespondWithError(w, http.StatusInternalServerError, "Error al obetener pedidos de perdidas.")
		return
	}

	// Suma total de lo vendido y Suma total del costo de lo vendido para perdidas
	for _, valueP := range pedidosPerdidas {
		total_pedidos_perdidas += 1
		for _, valP := range valueP.Productos {
			total_costo_pedidos_perdidas += valP.Precio
		}
	}

	var pedidos models.Pedidos

	params := bson.M{"$or": []bson.M{bson.M{"estado": "entregado"}, bson.M{"estado": "perdida"}},
		"usuario_designado.email": usuario,
		"fecha_entrega2":          bson.M{"$gt": fecha1, "$lt": fecha2}}

	err := database.GetCollection(database.PEDIDOS).Find(params).Sort("+numero").All(&pedidos)
	if err != nil {
		utils.RespondWithError(w, http.StatusInternalServerError, "Error al obetener pedidos entregados.")
		return
	}

	// Suma total de lo vendido y Suma total del costo de lo vendido
	for _, value := range pedidos {
		total_ventas += value.Total
		total_costo_envio += value.CostoEnvio
		for _, val := range value.Productos {
			total_costos += val.Costo
		}
	}

	// Obetener gastos

	total_gastos = obtenerGastos(fecha1, fecha2, w)

	// Suma total de lo vendido menos el costo ( utilidad )
	utilidad = total_ventas - total_costos

	// 30% de la utilidad
	utilidad_30 = (utilidad * 30) / 100
	// 50% de la utilidad
	utilidad_50 = (utilidad * 50) / 100
	// 70% de la utilidad
	utilidad_70 = (utilidad * 70) / 100

	total_ganacia_sanus = (utilidad + total_costo_envio) - (total_costo_pedidos_perdidas + total_gastos)

	// Cantidad de pedidos para esa fecha con el usuario
	response.Cantidad_pedidos = len(pedidos)
	response.Total_ventas = total_ventas
	response.Total_costos = total_costos
	response.Total_gastos = total_gastos
	response.Total_costos_envio = total_costo_envio
	response.Utilidad = utilidad
	response.Utilidad_30 = utilidad_30
	response.Utilidad_50 = utilidad_50
	response.Utilidad_70 = utilidad_70
	response.Total_pedidos_perdidas = total_pedidos_perdidas
	response.Total_costo_pedidos_perdidas = total_costo_pedidos_perdidas
	response.Total_ganacia_sanus = total_ganacia_sanus

	response.Pedidos = pedidos

	utils.RespondWithJSON(w, http.StatusOK, response)
}

func GetTotalPedidosPorDia(w http.ResponseWriter, r *http.Request) {

	isAuth := authentication.ValidateToken(r)
	if (isAuth != "") {
		utils.RespondWithError(w, http.StatusUnauthorized, isAuth)
		return
	}

	isAdmin := authentication.IsUserAdmin(r);
	if isAdmin == false {
		utils.RespondWithError(w, http.StatusUnauthorized, "No tiene permisos para realizar esta accion")
		return
	}

	defer r.Body.Close()
	var inputP models.FinanzasRequest
	if err := json.NewDecoder(r.Body).Decode(&inputP); err != nil {
		utils.RespondWithError(w, http.StatusInternalServerError, "Error al leer el body.")
		return
	}

	fecha1, fecha2, _ := utils.SetFechaTimeToZero(inputP.Fecha_inicio, inputP.Fecha_fin)

	var pedidos models.Pedidos
	paramsP := bson.M{"estado": "pendiente", "usuario_designado.email": inputP.UsuarioEmail, "fecha_entrega2": bson.M{"$gt": fecha1, "$lt": fecha2}}
	errP := database.GetCollection(database.PEDIDOS).Find(paramsP).All(&pedidos)
	if errP != nil {
		utils.RespondWithError(w, http.StatusInternalServerError, "Error al obetener pedidos de perdidas.")
		return
	}

	var c = []models.OutputTotalPedidos{}
	for _, value := range pedidos {
		for _, prod := range value.Productos {
			var a models.OutputTotalPedidos;
			a.Cantidad_prod = prod.Cantidad
			a.Nombre_prod = prod.Nombre
			c = append(c, a)
		}
	}
	utils.RespondWithJSON(w, http.StatusOK, c)

}

func GetPedidosDelivery(w http.ResponseWriter, r *http.Request) {

	isAuth := authentication.ValidateToken(r)
	if (isAuth != "") {
		utils.RespondWithError(w, http.StatusUnauthorized, isAuth)
		return
	}

	isAdmin := authentication.IsUserAdmin(r);
	if isAdmin == false {
		utils.RespondWithError(w, http.StatusUnauthorized, "No tiene permisos para realizar esta accion")
		return
	}

	defer r.Body.Close()
	var inputP models.FinanzasRequest
	if err := json.NewDecoder(r.Body).Decode(&inputP); err != nil {
		utils.RespondWithError(w, http.StatusInternalServerError, "Error al leer el body.")
		return
	}

	fecha1, fecha2, _ := utils.SetFechaTimeToZero(inputP.Fecha_inicio, inputP.Fecha_fin)

	var pedidos models.Pedidos
	paramsP := bson.M{"estado": "pendiente", "usuario_designado.email": inputP.UsuarioEmail, "tipo_despacho": "Domicilio",
		"fecha_entrega2": bson.M{"$gt": fecha1, "$lt": fecha2}}
	errP := database.GetCollection(database.PEDIDOS).Find(paramsP).All(&pedidos)
	if errP != nil {
		utils.RespondWithError(w, http.StatusInternalServerError, "Error al obetener pedidos de delivery.")
		return
	}

	var e = []models.OutputDetalleDelivery{}

	for _, b := range pedidos {
		var c models.OutputDetalleDelivery
		c.Nombre = b.Nombre
		c.Direccion = b.Direccion
		c.Telefono = b.Telefono
		c.Costo_envio = b.CostoEnvio
		c.Pagado = b.Pagado
		e = append(e, c)
	}
	utils.RespondWithJSON(w, http.StatusOK, e)

}

func UpdatePedido2(w http.ResponseWriter, r *http.Request) {

	var pedidos []models.Pedido
	err := database.GetCollection(database.PEDIDOS).Find(bson.M{"ciudad": "Santiago"}).All(&pedidos)
	if err != nil {
		utils.RespondWithError(w, http.StatusInternalServerError, "No se encontraron registros en la coleccion.")
		return
	}

	var c = []bson.ObjectId{}
	for _, value := range pedidos {
		c = append(c, value.ID)
	}

	var pedido_data models.Pedido
	pedido_data.FechaCreacion = utils.GetFechaActualChile()

	for _, value2 := range c {
		//idDeserialize := bson.ObjectIdHex(value2);
		document := bson.M{"_id": value2}
		change := bson.M{"$set": pedido_data}

		error := database.GetCollection(database.PEDIDOS).Update(document, change)

		if (error != nil) {
			log.Fatal(error)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	}

	w.Header().Set("Access-Control-Allow-Origin", "*")

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(pedido_data)
}

// Metodo que no se usa en la app, solo para sacar los costos de los productos

func ObetenerInfoCostos(w http.ResponseWriter, r *http.Request) {

	var productos models.Productos
	err := database.GetCollection(database.PRODUCTOS).Find(nil).All(&productos)
	if err != nil {
		utils.RespondWithError(w, http.StatusInternalServerError, "No se encontraron registros en la coleccion.")
		return
	}

	var res models.PedidoResponseCosto
	var response []models.PedidoResponseCosto

	for _, value := range productos {

		res.NombreProducto = value.Nombre
		res.Precio = value.Precio
		res.Costo = value.Costo
		res.Ganancia = (value.Precio - value.Costo)
		response = append(response, res)
	}
	w.Header().Set("Access-Control-Allow-Origin", "*")

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(response)
}

// Funcion que solo sirve para agregar campo fecha_entrega2  Time solo se deberia usar una vez

func CambiarAFechaTime(w http.ResponseWriter, r *http.Request) {

	var pedidos []models.Pedido

	err := database.GetCollection(database.PEDIDOS).Find(nil).All(&pedidos)
	if err != nil {
		utils.RespondWithError(w, http.StatusInternalServerError, "No se encontraron registros en la coleccion.")
		return
	}

	for _, value := range pedidos {

		fechaTime, _ := utils.ParseFecha(value.FechaEntrega)

		document := bson.M{"_id": value.ID}
		value.FechaEntrega2 = fechaTime
		change := bson.M{"$set": value}

		error := database.GetCollection(database.PEDIDOS).Update(document, change)

		if (error != nil) {
			log.Fatal(error)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	}
	w.WriteHeader(http.StatusOK)

}
