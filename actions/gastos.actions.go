package actions

import (
	"encoding/json"
	"fmt"
	"gitlab.com/skolldevs-backend/sanus-api/authentication"
	"gitlab.com/skolldevs-backend/sanus-api/database"
	"gitlab.com/skolldevs-backend/sanus-api/models"
	"gitlab.com/skolldevs-backend/sanus-api/utils"
	"gopkg.in/mgo.v2/bson"
	"net/http"
	"time"
)

func CrearGasto(w http.ResponseWriter, r *http.Request) {

	isAuth := authentication.ValidateToken(r)
	if (isAuth != "") {
		utils.RespondWithError(w, http.StatusUnauthorized, isAuth)
		return
	}

	isAdmin := authentication.IsUserAdmin(r);
	if isAdmin == false{
		utils.RespondWithError(w, http.StatusUnauthorized, "No es administrador")
		return
	}

	defer r.Body.Close()
	var gastos models.MesGasto
	if err := json.NewDecoder(r.Body).Decode(&gastos); err != nil {
		utils.RespondWithError(w, http.StatusInternalServerError, "Error al decodificar parametros de entrada.")
		return
	}

	existe, fechaMensual := checkIfExistDate(gastos.Mes, gastos.Anio, w)

	if (existe) {

		document := bson.M{"fecha_mensual": fechaMensual}
		change := bson.M{"$set": gastos}

		error := database.GetCollection(database.GASTOS).Update(document, change)

		if (error != nil) {
			fmt.Println(error)
			utils.RespondWithError(w, http.StatusInternalServerError, "Error al actualizar gasto.")
			return
		}

		utils.RespondWithJSON(w, http.StatusOK, gastos)

	} else {
		gastos.ID = bson.NewObjectId()
		gastos.Create_date, _ = utils.GetDateTimeActual()
		gastos.Fecha_mensual = fechaMensual

		if err := database.GetCollection(database.GASTOS).Insert(gastos); err != nil {
			utils.RespondWithError(w, http.StatusInternalServerError, "Error al crear gasto.")
			return
		}
		utils.RespondWithJSON(w, http.StatusCreated, gastos)
	}
}

func checkIfExistDate(mes string, anio string, w http.ResponseWriter) (bool, time.Time) {

	fecha := anio + "-" + mes + "-01 00:00:00"
	fechaParsed, _ := utils.ParseFecha(fecha)

	params := bson.M{"fecha_mensual": fechaParsed}
	count, err := database.GetCollection(database.GASTOS).Find(params).Count()
	if err != nil {
		utils.RespondWithError(w, http.StatusInternalServerError, "Error al contar fecha mensual.")
	}

	if (count > 0) {
		return true, fechaParsed
	} else {
		return false, fechaParsed
	}
}

func GetAllGastos(w http.ResponseWriter, r *http.Request) {

	var gastos models.MesGasto
	err := database.GetCollection(database.GASTOS).Find(nil).Sort("+_id").All(&gastos)
	if err != nil {
		utils.RespondWithError(w, http.StatusInternalServerError, "No se encontraron registros en la coleccion.")
		return
	}
	utils.RespondWithJSON(w, http.StatusOK, gastos)
}

func createDate(mes string, anio string)time.Time{
	fechaParsed, _ := utils.ParseFecha(anio + "-" + mes + "-01 00:00:00")
	return fechaParsed
}

func GetGastosByMonthAndYear(w http.ResponseWriter, r *http.Request) {

	isAuth := authentication.ValidateToken(r)
	if (isAuth != "") {
		utils.RespondWithError(w, http.StatusUnauthorized, isAuth)
		return
	}

	isAdmin := authentication.IsUserAdmin(r);
	if isAdmin == false{
		utils.RespondWithError(w, http.StatusUnauthorized, "No es administrador")
		return
	}

	defer r.Body.Close()
	var fechasRqst models.GastoFechasRequest
	if err := json.NewDecoder(r.Body).Decode(&fechasRqst); err != nil {
		utils.RespondWithError(w, http.StatusInternalServerError, "Error al decodificar parametros de entrada.")
		return
	}
	fecha := createDate(fechasRqst.Mes, fechasRqst.Anio)

	var gastos []models.MesGasto
	params := bson.M{"fecha_mensual": fecha}
	err := database.GetCollection(database.GASTOS).Find(params).All(&gastos)
	if err != nil {
		utils.RespondWithError(w, http.StatusInternalServerError, "Error al obetener pedidos entregados.")
		return
	}

	utils.RespondWithJSON(w, http.StatusOK, gastos)
}


