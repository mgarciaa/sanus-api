package actions

import (
	"encoding/json"
	"gitlab.com/skolldevs-backend/sanus-api/authentication"
	"gitlab.com/skolldevs-backend/sanus-api/database"
	"gitlab.com/skolldevs-backend/sanus-api/models"
	"gitlab.com/skolldevs-backend/sanus-api/utils"
	"gopkg.in/mgo.v2/bson"
	"net/http"
	"time"
)

var cUsuario = database.GetCollection(database.USUARIOS)

func CrearUsuario(w http.ResponseWriter, r *http.Request) {

	defer r.Body.Close()
	var usuario models.User
	if err := json.NewDecoder(r.Body).Decode(&usuario); err != nil {
		utils.RespondWithError(w, http.StatusInternalServerError, "Error al decodificar parametros de entrada.")
		return
	}
	usuario.ID = bson.NewObjectId()
	usuario.Create_date = time.Now()
	//hashea password
	passHasheada := utils.HashearPassword(usuario.Password)

	usuario.PasswordHash = passHasheada
	usuario.Password = ""
	if err := cUsuario.Insert(usuario); err != nil {
		utils.RespondWithError(w, http.StatusInternalServerError, "Error al crear Usuario.")
		return
	}
	utils.RespondWithJSON(w, http.StatusCreated, usuario)
}
//
func GetAllRolUsuarios(w http.ResponseWriter, r *http.Request) {

	isAuth := authentication.ValidateToken(r)
	if (isAuth != "") {
		utils.RespondWithError(w, http.StatusUnauthorized, isAuth)
		return
	}

	isAdmin := authentication.IsUserAdmin(r);
	if isAdmin == false{
		utils.RespondWithError(w, http.StatusUnauthorized, "No es administrador")
		return
	}

	var usuarios []models.UserResponse
	err := cUsuario.Find(bson.M{"estado": 1}).All(&usuarios)
	if err != nil {
		utils.RespondWithError(w, http.StatusInternalServerError, "Error al buscar los usuarios.")
		return
	}
	utils.RespondWithJSON(w, http.StatusOK, usuarios)
}

//func UpdateUsuarioEndpoint(w http.ResponseWriter, r *http.Request) {
//
//	isAuth := authentication.ValidateToken(r)
//	if (isAuth != "") {
//		utils.RespondWithError(w, http.StatusUnauthorized, isAuth)
//		return
//	}
//
//	params := mux.Vars(r)
//	UsuarioID := params["id"]
//
//	if !bson.IsObjectIdHex(UsuarioID) {
//		utils.RespondWithError(w, http.StatusBadRequest, "El id no es un ObjectIdHex valido.")
//		return
//	}
//	decoder := json.NewDecoder(r.Body)
//	var Usuario_data models.User
//
//	err := decoder.Decode(&Usuario_data)
//	if (err != nil) {
//		utils.RespondWithError(w, http.StatusInternalServerError, "Error en el envio de parametros entrada de Usuario.")
//		return
//	}
//	defer r.Body.Close()
//
//	idDeserialize := bson.ObjectIdHex(UsuarioID);
//	Usuario_data.Mod_date = time.Now()
//	Usuario_data.Create_date = Usuario_data.Create_date
//
//	document := bson.M{"_id": idDeserialize}
//	change := bson.M{"$set": Usuario_data}
//
//	error := cUsuario.Update(document, change)
//
//	if (error != nil) {
//		fmt.Println(error)
//		utils.RespondWithError(w, http.StatusInternalServerError, "Error al actualizar Usuario.")
//		return
//	}
//	utils.RespondWithJSON(w, http.StatusOK, Usuario_data)
//}
//
//func GetUsuariosPorEstadodEndpoint(w http.ResponseWriter, r *http.Request) {
//
//	isAuth := authentication.ValidateToken(r)
//	if (isAuth != "") {
//		utils.RespondWithError(w, http.StatusUnauthorized, isAuth)
//		return
//	}
//
//	params := mux.Vars(r)
//	status := params["status"]
//	var Usuarios []models.User
//
//	var idEstado, erro = strconv.Atoi(status)
//	if erro != nil {
//		utils.RespondWithError(w, http.StatusInternalServerError, "Error al convertir parametro int.")
//	}
//	err := cUsuario.Find(bson.M{"estado": idEstado}).All(&Usuarios)
//	if (err != nil) {
//		utils.RespondWithError(w, http.StatusNotFound, "No se encontró el registro.")
//	}
//	utils.RespondWithJSON(w, http.StatusOK, Usuarios)
//}
//
//func DeleteUsuarioEndpoint(w http.ResponseWriter, r *http.Request) {
//
//	isAuth := authentication.ValidateToken(r)
//	if (isAuth != "") {
//		utils.RespondWithError(w, http.StatusUnauthorized, isAuth)
//		return
//	}
//
//	defer r.Body.Close()
//	var Usuario models.User
//	if err := json.NewDecoder(r.Body).Decode(&Usuario); err != nil {
//		utils.RespondWithError(w, http.StatusBadRequest, "Invalid request payload")
//		return
//	}
//	error := cUsuario.Remove(Usuario)
//	if (error != nil) {
//		fmt.Println(error)
//		utils.RespondWithError(w, http.StatusInternalServerError, "Error al eliminar Usuario.")
//		return
//	}
//	utils.RespondWithJSON(w, http.StatusOK, map[string]string{"result": "success"})
//}
//
//func GetUsuarioByIdEndpoint(w http.ResponseWriter, r *http.Request) {
//
//	isAuth := authentication.ValidateToken(r)
//	if (isAuth != "") {
//		utils.RespondWithError(w, http.StatusUnauthorized, isAuth)
//		return
//	}
//
//	params := mux.Vars(r)
//	idProd := params["id"]
//	var Usuario models.User
//
//	if !bson.IsObjectIdHex(idProd) {
//		utils.RespondWithError(w, http.StatusBadRequest, "El id no es un ObjectIdHex valido.")
//		return
//	}
//	err := cUsuario.FindId(bson.ObjectIdHex(idProd)).One(&Usuario)
//	if (err != nil) {
//		utils.RespondWithError(w, http.StatusNotFound, "No se encontró el registro.")
//	}
//	utils.RespondWithJSON(w, http.StatusOK, Usuario)
//}
