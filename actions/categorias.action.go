package actions
//
//import (
//	"encoding/json"
//	"fmt"
//	"github.com/gorilla/mux"
//	"gitlab.com/skolldevs-backend/sanus-api/authentication"
//	"gitlab.com/skolldevs-backend/sanus-api/database"
//	"gitlab.com/skolldevs-backend/sanus-api/models"
//	"gitlab.com/skolldevs-backend/sanus-api/utils"
//	"gopkg.in/mgo.v2/bson"
//	"net/http"
//)
//
//func CreateCategoriaEndPoint(w http.ResponseWriter, r *http.Request) {
//
//	isAuth := authentication.ValidateToken(r)
//	if (isAuth != "") {
//		utils.RespondWithError(w, http.StatusUnauthorized, isAuth)
//		return
//	}
//
//	defer r.Body.Close()
//	var categoria models.Categoria
//	if err := json.NewDecoder(r.Body).Decode(&categoria); err != nil {
//		utils.RespondWithError(w, http.StatusInternalServerError, "Error al leer el body.")
//		return
//	}
//
//	dbName := utils.GetNombreDatabase(r.RequestURI)
//
//	categoria.ID = bson.NewObjectId();
//	if err := database.GetCollection(dbName, database.CATEGORIAS).Insert(categoria); err != nil {
//		utils.RespondWithError(w, http.StatusBadRequest, "No se pudo insertar el resgistro.")
//		return
//	}
//	utils.RespondWithJSON(w, http.StatusCreated, categoria)
//}
//
//func GetAllCategoriasEndPoint(w http.ResponseWriter, r *http.Request) {
//
//	var categorias models.Categorias
//	err := database.GetCollection(database.CATEGORIAS).Find(nil).Sort("+nombre").All(&categorias)
//	if err != nil {
//		utils.RespondWithError(w, http.StatusInternalServerError, "No se encontraron registros en la coleccion.")
//		return
//	}
//	utils.RespondWithJSON(w, http.StatusOK, categorias)
//}
//
//func GetAllCategoriasFEndPoint(w http.ResponseWriter, r *http.Request) {
//
//	dbName := utils.GetDominioByHeader(r.Header.Get("Origin"))
//	var categorias models.Categorias
//	err := database.GetCollection(dbName, database.CATEGORIAS).Find(nil).Sort("+nombre").All(&categorias)
//	if err != nil {
//		utils.RespondWithError(w, http.StatusInternalServerError, "No se encontraron registros en la coleccion.")
//		return
//	}
//	utils.RespondWithJSON(w, http.StatusOK, categorias)
//}
//
//func GetCategoriaByIdEndpoint(w http.ResponseWriter, r *http.Request) {
//	params := mux.Vars(r)
//	idCate := params["id"]
//	var categoria models.Categoria
//
//	if !bson.IsObjectIdHex(idCate) {
//		utils.RespondWithError(w, http.StatusBadRequest, "El id no es un ObjectIdHex valido.")
//		return
//	}
//
//	dbName := utils.GetNombreDatabase(r.RequestURI)
//
//	err := database.GetCollection(dbName, database.CATEGORIAS).FindId(bson.ObjectIdHex(idCate)).One(&categoria)
//	if (err != nil) {
//		utils.RespondWithError(w, http.StatusNotFound, "No se encontró el registro.")
//	}
//	utils.RespondWithJSON(w, http.StatusOK, categoria)
//}
//
//func UpdateCategoriaEndpoint(w http.ResponseWriter, r *http.Request) {
//
//	isAuth := authentication.ValidateToken(r)
//	if (isAuth != "") {
//		utils.RespondWithError(w, http.StatusUnauthorized, isAuth)
//		return
//	}
//
//	params := mux.Vars(r)
//	categoriaID := params["id"]
//
//	if !bson.IsObjectIdHex(categoriaID) {
//		utils.RespondWithError(w, http.StatusBadRequest, "El id no es un ObjectIdHex valido.")
//		return
//	}
//	decoder := json.NewDecoder(r.Body)
//	var categoria_data models.Categoria
//
//	err := decoder.Decode(&categoria_data)
//	if (err != nil) {
//		utils.RespondWithError(w, http.StatusInternalServerError, "Error en el envio de parametros entrada de Categoria.")
//		return
//	}
//	defer r.Body.Close()
//	idDeserialize := bson.ObjectIdHex(categoriaID);
//	document := bson.M{"_id": idDeserialize}
//	change := bson.M{"$set": categoria_data}
//
//	dbName := utils.GetNombreDatabase(r.RequestURI)
//	error := database.GetCollection(dbName, database.CATEGORIAS).Update(document, change)
//
//	if (error != nil) {
//		fmt.Println(error)
//		utils.RespondWithError(w, http.StatusInternalServerError, "Error al actualizar categoria.")
//		return
//	}
//	utils.RespondWithJSON(w, http.StatusOK, categoria_data)
//}
//
//func DeleteCategoriaEndpoint(w http.ResponseWriter, r *http.Request) {
//
//	isAuth := authentication.ValidateToken(r)
//	if (isAuth != "") {
//		utils.RespondWithError(w, http.StatusUnauthorized, isAuth)
//		return
//	}
//
//	defer r.Body.Close()
//	var categoria models.Categoria
//	if err := json.NewDecoder(r.Body).Decode(&categoria); err != nil {
//		utils.RespondWithError(w, http.StatusBadRequest, "Invalid request payload")
//		return
//	}
//	dbName := utils.GetNombreDatabase(r.RequestURI)
//	error := database.GetCollection(dbName, database.CATEGORIAS).Remove(categoria)
//	if (error != nil) {
//		fmt.Println(error)
//		utils.RespondWithError(w, http.StatusInternalServerError, "Error al eliminar categoria.")
//		return
//	}
//	utils.RespondWithJSON(w, http.StatusOK, map[string]string{"result": "Categoria eliminada"})
//}
//
//func ExisteCategoria(w http.ResponseWriter, r *http.Request) {
//
//	params := mux.Vars(r)
//	idCate := params["id"]
//
//	dbName := utils.GetNombreDatabase(r.RequestURI)
//	existeCategoria, err := database.GetCollection(dbName, database.CATEGORIAS).Find(bson.M{"id_categoria": idCate}).Count()
//
//	if (err != nil) {
//		utils.RespondWithError(w, http.StatusInternalServerError, "Error al buscar categoria.")
//		return
//	}
//
//	if (existeCategoria > 0) {
//		utils.RespondWithJSON(w, http.StatusOK, true)
//	} else {
//		utils.RespondWithJSON(w, http.StatusOK, false)
//	}
//}
