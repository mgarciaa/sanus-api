package routes

import (
	"github.com/gorilla/mux"
	"gitlab.com/skolldevs-backend/sanus-api/utils"
	"net/http"
)

func addFileRoutes(r *mux.Router) {

	r.HandleFunc("/upload", utils.UploadFile).Methods(http.MethodPost)
	r.PathPrefix("/files/").Handler(http.StripPrefix("/files/", http.FileServer(http.Dir(utils.GeFilesRoutes()))))
}
