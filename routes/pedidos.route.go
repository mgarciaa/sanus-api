package routes

import (
	"github.com/gorilla/mux"
	"gitlab.com/skolldevs-backend/sanus-api/actions"
	"net/http"
)

func addPedidosRoutes(r *mux.Router) {

	r.HandleFunc("/pedidos", actions.GetAllPedidos).Methods(http.MethodGet)
	r.HandleFunc("/pedido", actions.CreatePedido).Methods(http.MethodPost)
	r.HandleFunc("/pedido/{id}", actions.FindPedidoByID).Methods(http.MethodGet)
	r.HandleFunc("/pedido/{id}", actions.UpdatePedido).Methods(http.MethodPut)
	r.HandleFunc("/pedidosFinanzas", actions.GetFinanzasByFechaAndUser).Methods(http.MethodPost)
	r.HandleFunc("/pedidoschange", actions.UpdatePedido2).Methods(http.MethodGet)
	r.HandleFunc("/pedidosTotales", actions.GetTotalPedidosPorDia).Methods(http.MethodPost)
	r.HandleFunc("/pedidosDelivery", actions.GetPedidosDelivery).Methods(http.MethodPost)

	// Recibe por query params  estado, paginas, limit
	r.HandleFunc("/pedidoEstado", actions.GetPedidosByEstado).Methods(http.MethodGet)


	r.HandleFunc("/cambiarAFechaTime", actions.CambiarAFechaTime).Methods(http.MethodGet)


}
