package routes

import (
	"github.com/gorilla/mux"
	"gitlab.com/skolldevs-backend/sanus-api/actions"
	"net/http"
)

func addGastosRoutes(r *mux.Router) {
	// Handlers productos
	r.HandleFunc("/gastos", actions.CrearGasto).Methods(http.MethodPost)
	r.HandleFunc("/gastos", actions.GetAllGastos).Methods(http.MethodGet)
	r.HandleFunc("/gastos/fechas", actions.GetGastosByMonthAndYear).Methods(http.MethodPost)
}






