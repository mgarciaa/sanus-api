package routes

import "github.com/gorilla/mux"

func AddRoutes(r *mux.Router) {

	addAuthenticationRoutes(r)
	addCategoriaRoutes(r)
	addPedidosRoutes(r)
	addProductoRoutes(r)
	addUsuarioRoutes(r)
	fechasRoutes(r)
	addGastosRoutes(r)
	addFileRoutes(r)
}
