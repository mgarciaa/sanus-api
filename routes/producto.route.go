package routes

import (
	"github.com/gorilla/mux"
	"gitlab.com/skolldevs-backend/sanus-api/actions"
	"net/http"
)

func addProductoRoutes(r *mux.Router) {
	// Handlers productos
	r.HandleFunc("/producto", actions.CreateProducto).Methods(http.MethodPost)
	r.HandleFunc("/productos", actions.GetAllProductos).Methods(http.MethodGet)
	r.HandleFunc("/productos/categoria/{categoria}", actions.GetProductosByCategoria).Methods(http.MethodGet)
	r.HandleFunc("/producto/{id}", actions.UpdateProducto).Methods(http.MethodPut)
	r.HandleFunc("/producto/search/{texto}", actions.FindProductosByWords).Methods(http.MethodGet)
	r.HandleFunc("/productos/destacados", actions.GetProductosDestacados).Methods(http.MethodGet)


	r.HandleFunc("/costos", actions.ObetenerInfoCostos).Methods(http.MethodGet)
	r.HandleFunc("/conteoProductos", actions.GetConteoProductos).Methods(http.MethodGet)

}






