package routes

import (
	"github.com/gorilla/mux"
	"gitlab.com/skolldevs-backend/sanus-api/actions"
	"net/http"
)
func fechasRoutes(r *mux.Router) {
	r.HandleFunc("/fechas", actions.GetFechas).Methods(http.MethodGet)
}
