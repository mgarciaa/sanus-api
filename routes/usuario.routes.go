package routes

import (
	"github.com/gorilla/mux"
	"gitlab.com/skolldevs-backend/sanus-api/actions"
	"net/http"
)

func addUsuarioRoutes(r *mux.Router) {
	// Handlers Usuario
	r.HandleFunc("/usuario", actions.CrearUsuario).Methods(http.MethodPost)
	r.HandleFunc("/usuarios", actions.GetAllRolUsuarios).Methods(http.MethodGet)
	//r.HandleFunc("/usuario/{id}", actions.GetUsuarioByIdEndpoint).Methods(http.MethodGet)
	//r.HandleFunc("/usuario/{id}", actions.UpdateUsuarioEndpoint).Methods(http.MethodPut)
	//r.HandleFunc("/usuarios/{status}", actions.GetUsuariosPorEstadodEndpoint).Methods(http.MethodGet)
	//r.HandleFunc("/usuario", actions.DeleteUsuarioEndpoint).Methods(http.MethodDelete)
}
