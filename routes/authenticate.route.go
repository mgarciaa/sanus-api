package routes

import (
	"github.com/gorilla/mux"
	"gitlab.com/skolldevs-backend/sanus-api/authentication"
	"net/http"
)

func addAuthenticationRoutes(r *mux.Router) {
	//Login
	r.HandleFunc("/login", authentication.Login).Methods(http.MethodPost)
	r.HandleFunc("/validate", authentication.IsValidToken).Methods(http.MethodGet)
}
