package authentication

// se firman los token con llave privada
// se verifica con llave publica

import (
	"crypto/rsa"
	"encoding/json"
	"errors"
	"github.com/dgrijalva/jwt-go"
	"github.com/dgrijalva/jwt-go/request"
	"github.com/labstack/echo"
	"gitlab.com/skolldevs-backend/sanus-api/database"
	"gitlab.com/skolldevs-backend/sanus-api/models"
	"gitlab.com/skolldevs-backend/sanus-api/utils"
	"gopkg.in/mgo.v2/bson"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

var cUsuario = database.GetCollection(database.USUARIOS)

var (
	privateKey *rsa.PrivateKey
	publicKey  *rsa.PublicKey
)

const (
	privateKeyFileName = "privateToken.rsa"
	publicKeyFileName  = "public.rsa.pub"
)

func init() {
	workDir := utils.GetUserWorkDir()
	privateBytes, err := ioutil.ReadFile(workDir + privateKeyFileName)
	if err != nil {
		log.Fatal("no se pudo leer clave privada")
	}

	publicBytes, err := ioutil.ReadFile(workDir + publicKeyFileName)
	if err != nil {
		log.Fatal("no se pudo leer clave publica")
	}

	privateKey, err = jwt.ParseRSAPrivateKeyFromPEM(privateBytes)
	if err != nil {
		log.Fatal("No se pudo tratar la privada")
	}

	publicKey, err = jwt.ParseRSAPublicKeyFromPEM(publicBytes)
	if err != nil {
		log.Fatal("No se pudo tratar la public")
	}
}

//funcion para generar token

func GenerateJWT(user models.User) (string) {
	claims := models.Claim{
		Rol:    user.Role,
		Nombre: user.Nombre,
		Email:  user.Email,
		Ciudad:  user.Ciudad,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: utils.GetFechaActualChile().Add(time.Hour * 120).Unix(),
			Issuer:    "sanus",
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodPS256, claims)
	result, err := token.SignedString(privateKey)
	if err != nil {
		log.Fatal("No se pudo firmar el token")
	}
	return result
}

func Login(w http.ResponseWriter, r *http.Request) {
	var userUknown models.User
	err := json.NewDecoder(r.Body).Decode(&userUknown)
	if (err != nil) {
		utils.RespondWithError(w, http.StatusInternalServerError, "Error al leer el body.")
		return
	}

	var user models.User
	err = cUsuario.Find(bson.M{"usuario": userUknown.Usuario}).One(&user)
	if err != nil {
		utils.RespondWithError(w, http.StatusNotFound, "Usuario o clave incorrecta.")
		return
	}

	if user.Usuario == userUknown.Usuario && utils.ComparePassword(user.PasswordHash, userUknown.Password) {
		userUknown.Password = ":)"
		userUknown.Usuario = ""

		token := GenerateJWT(user)
		if err != nil {
			utils.RespondWithError(w, http.StatusInternalServerError, "Error al generar json de token.")
			return
		}
		//Se asigna token creado al objeto user
		user.Token = token
		user.PasswordHash = nil

		utils.RespondWithJSON(w, http.StatusOK, user)

	} else {
		utils.RespondWithError(w, http.StatusUnauthorized, "Usuario o clave incorrecta.")
	}
}

func ValidateToken(r *http.Request) string {
	token, err := request.ParseFromRequestWithClaims(r, request.OAuth2Extractor, &models.Claim{}, func(token *jwt.Token) (interface{}, error) {
		return publicKey, nil
	})
	if err != nil {
		switch err.(type) {
		case *jwt.ValidationError:
			vErr := err.(*jwt.ValidationError)
			switch vErr.Errors {
			case jwt.ValidationErrorExpired:
				return "Su token ha expirado."
			case jwt.ValidationErrorSignatureInvalid:
				return "Su token no coincide."
			default:
				return "Su token no es correcto."
			}
		default:
			return "Su token no es válido."
		}
	}
	if token.Valid {
		return ""
	} else {
		return "su token no es valido inautorizado"
	}
}

func IsUserAdmin(r *http.Request) bool {

	var token = GetTokenFromHeader(r)
	mapClaims, _ := GetClaimsFromToken(token)
	if mapClaims["rol"] == "ADMIN_ROLE" {
		return true
	} else {
		return false
	}
}

func GetUserEmailFromClaims(r *http.Request) string {
	var token = GetTokenFromHeader(r)
	mapClaims, _ := GetClaimsFromToken(token)
	return mapClaims["email"].(string)
}

func GetClaimsFromToken(tokenStr string) (jwt.MapClaims, error) {

	token, err := jwt.Parse(tokenStr, func(token *jwt.Token) (interface{}, error) {
		// check token signing method etc
		return publicKey, nil
	})
	if err != nil {
		return nil, err
	}
	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		return claims, nil
	} else {
		log.Printf("Invalid JWT Token")
		return nil, errors.New("Token invalido")
	}
}

func GetTokenFromHeader(request *http.Request) string {
	token := request.Header.Get(echo.HeaderAuthorization)
	return token
}

func IsValidToken(w http.ResponseWriter, r *http.Request) {
	token, err := request.ParseFromRequestWithClaims(r, request.OAuth2Extractor, &models.Claim{}, func(token *jwt.Token) (interface{}, error) {
		return publicKey, nil
	})
	if err != nil {
		switch err.(type) {
		case *jwt.ValidationError:
			vErr := err.(*jwt.ValidationError)
			switch vErr.Errors {
			case jwt.ValidationErrorExpired:
				utils.RespondWithError(w, http.StatusUnauthorized, "false")
				return
			case jwt.ValidationErrorSignatureInvalid:
				utils.RespondWithError(w, http.StatusUnauthorized, "false")
				return
			default:
				utils.RespondWithError(w, http.StatusUnauthorized, "false")
				return
			}
		default:
			utils.RespondWithError(w, http.StatusUnauthorized, "false")
			return
		}
	}
	if token.Valid {
		utils.RespondWithJSON(w, http.StatusOK, true)
	} else {
		utils.RespondWithError(w, http.StatusUnauthorized, "false")
		return
	}
}
