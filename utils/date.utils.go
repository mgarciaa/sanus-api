package utils

import (
	"github.com/jinzhu/now"
	"time"
)

func GetFechaActual() (fecha1, fecha2, fechaFull string) {

	chile_location, err := time.LoadLocation("America/Santiago")
	if err != nil {
		// handle error
	}
	now_chile := time.Now().In(chile_location)
	fecha1 = (now.New(now_chile).BeginningOfDay()).Format("2006-01-02 15:04:05")
	fecha2 = (now.New(now_chile).EndOfDay()).Format("2006-01-02 15:04:05")
	fechaFull = (now_chile).Format("2006-01-02 15:04:05")
	return
}

func GetFechaActualChile() time.Time {
	chile_location, _ := time.LoadLocation("America/Santiago")
	return time.Now().In(chile_location)
}

func ParseFecha(fecha string) (fechaParsed time.Time, error bool) {

	if len(fecha) > 19 {
		fechaParsed, _ = time.Parse(time.RFC3339, fecha)
		error = false
		return
	} else {
		layout := "2006-01-02 15:04:05"
		fechaParsed, _ = time.Parse(layout, fecha)
		error = false
		return
	}
}

func SetFechaTimeToZero(fecha_inicio string, fecha_fin string) (date_inicio time.Time, date_fin time.Time, error bool) {

	fecha1, _ := ParseFecha(fecha_inicio)
	fecha2, _ := ParseFecha(fecha_fin)

	date_inicio = now.New(fecha1).BeginningOfDay()
	date_fin = now.New(fecha2).EndOfDay()
	return
}

func GetDateTimeActual() (fecha time.Time, error bool) {

	chile_location, err := time.LoadLocation("America/Santiago")
	if err != nil {
		// handle error
	}
	fecha = time.Now().In(chile_location)
	return
}
