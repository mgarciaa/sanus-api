package utils

import (
	"gitlab.com/skolldevs-backend/sanus-api/constants"
	"log"
	"os/user"
	"runtime"
)

const (
	windowsPathSeparator = "\\"
	linuxPathSeparator   = "/"
)

var (
	rutaTrabajoUser string
	rutaImagenes    string
	rutaFiles       string
)

func GetUserWorkDir() string {
	if rutaTrabajoUser == "" {
		usr := getUserHome()
		separador := GetSeparator()
		rutaTrabajoUser = usr.HomeDir + separador + "work" + separador + constants.API_PATH + separador
	}
	return rutaTrabajoUser;
}

func GetImageRoute(carpetaClientes string) string {
	if rutaImagenes == "" {
		separador := GetSeparator()
		rutaImagenes = GeFilesRoutes() + carpetaClientes + separador
	}
	return rutaImagenes
}

func GeFilesRoutes() string {
	if rutaFiles == "" {
		separador := GetSeparator()
		rutaFiles = GetUserWorkDir() + "files" + separador
	}
	return rutaFiles
}

func GetSeparator() string {
	if runtime.GOOS == "windows" {
		return windowsPathSeparator
	} else {
		return linuxPathSeparator
	}
}

func getUserHome() *user.User {
	usr, err := user.Current()
	if err != nil {
		log.Fatal(err.Error())
	}
	return usr
}
