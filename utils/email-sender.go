package utils

import (
	"bytes"
	"errors"
	"github.com/labstack/gommon/log"
	"github.com/leekchan/accounting"
	"gitlab.com/skolldevs-backend/sanus-api/constants"
	"gitlab.com/skolldevs-backend/sanus-api/models"
	"gopkg.in/gomail.v2"
	"html/template"
)

func SendEmail(mailConfig models.MailConfig) error {

	t := template.New(mailConfig.TemplateName)
	var tpl bytes.Buffer

	var err error
	t.Funcs(addFunctionsToTemplate())
	t, err = t.ParseFiles(mailConfig.TemplateUrl)
	if err != nil {
		log.Info("No se logro parsear Template")
		return errors.New("No se logro parsear Template")
	}

	if err := t.Execute(&tpl, mailConfig); err != nil {
		log.Info("Error al parsear datos del template")
		return errors.New("No se logro parsear Template")
	}

	result := tpl.String()
	m := gomail.NewMessage()
	m.SetHeader("From", constants.FROM)
	m.SetHeader("To", mailConfig.To)
	if mailConfig.Cc != "" {
		m.SetAddressHeader("Cc", mailConfig.Cc, mailConfig.CcName)
	}
	m.SetHeader("Subject", mailConfig.Subject)
	m.SetBody("text/html", result)

	//Embeber imagenes
	m.Embed(mailConfig.TemplateImageUrl)

	//Atachar imagenes
	//m.Attach("email-pedidos_tpl.html") // attach whatever you want

	d := gomail.NewDialer(constants.HOST, constants.PORT, constants.USERNAME, constants.PASSWORD)

	if err := d.DialAndSend(m); err != nil {
		log.Error(err.Error())
		return errors.New("No se pudo enviar el email")
	}

	return nil
}

func addFunctionsToTemplate() (map[string]interface{}) {
	var functions = make(map[string]interface{})
	multiply := func(a, b int) int { return a * b }

	formatMoney := func(monto int) string {
		ac := accounting.Accounting{Symbol: "$ ", Thousand: ".", Precision: 0, Decimal: ",",}
		return ac.FormatMoney(monto)
	}
	functions["multiply"] = multiply
	functions["formatMoney"] = formatMoney
	return functions
}
