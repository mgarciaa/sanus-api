package utils

import (
	"fmt"
	"github.com/nfnt/resize"
	"gopkg.in/mgo.v2/bson"
	"image/jpeg"
	"image/png"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"os"
	"strings"
)

func UploadFile(w http.ResponseWriter, r *http.Request) {

	file, handle, err := r.FormFile("file")
	if err != nil {
		fmt.Println(err)
		RespondWithError(w, http.StatusInternalServerError, "Error con el nombre de key del archivo.")
		return
	}
	handle.Filename = generaNewBsonId() + "-" + handle.Filename
	dbName := "test"
	defer file.Close()
	mimeType := handle.Header.Get("Content-Type")
	switch mimeType {
	case "image/jpeg":
		saveFile(w, file, handle, dbName)
	case "image/png":
		saveFile(w, file, handle, dbName)
	default:
		RespondWithError(w, http.StatusInternalServerError, "Formato del archivo invalido.")
		return
	}
}

func generaNewBsonId() string {
	var a = bson.NewObjectId();
	var b = a.Hex();
	return b
}

func saveFile(w http.ResponseWriter, file multipart.File, handle *multipart.FileHeader, dbName string) {
	data, err := ioutil.ReadAll(file)
	if err != nil {
		fmt.Println(err)
		RespondWithError(w, http.StatusInternalServerError, "Error al leer el archivo")
		return
	}

	urlUploadr := GetUserWorkDir() + "uploads/"

	CreateDirIfNotExist(urlUploadr)

	err = ioutil.WriteFile(urlUploadr+handle.Filename, data, 0666)
	if err != nil {
		fmt.Println(err)
		RespondWithError(w, http.StatusInternalServerError, "Error al escribir archivo al servidor")
		return
	}
	if (reSizeFile(handle.Filename, dbName)) {
		if (!DeleteFile(urlUploadr + handle.Filename)) {
			RespondWithError(w, http.StatusInternalServerError, "Error en DeleteFile")
			return
		}
	} else {
		RespondWithError(w, http.StatusInternalServerError, "Error en resize")
		return
	}

	RespondWithJSON(w, http.StatusOK, handle.Filename)
}

func reSizeFile(filename string, dbName string) (error bool) {
	URL_IMAGES := GetUserWorkDir() + "files/"
	file, err := os.Open(GetUserWorkDir() + "uploads/" + filename)
	if err != nil {
		fmt.Println("Error al abrir archivo para rezise")
		return false
	}
	// decode jpeg into image.Image
	arr := strings.Split(filename, ".")
	extension := strings.ToLower(arr[1])
	if (extension == "png") {

		img, err := png.Decode(file)
		if err != nil {
			fmt.Println("Error decodificando archivo para rezise png")
			return false
		}
		file.Close()
		// resize to width 1000 using Lanczos resampling
		// and preserve aspect ratio
		m := resize.Resize(1000, 0, img, resize.Lanczos3)

		CreateDirIfNotExist(URL_IMAGES + dbName)

		out, err := os.Create(URL_IMAGES + dbName + "/" + filename)
		if err != nil {
			fmt.Println("Error al escribiendo archivo en rezise")
			return false
		}
		defer out.Close()
		// write new image to file
		jpeg.Encode(out, m, nil)
		return true

	} else {
		img, err := jpeg.Decode(file)
		if err != nil {
			fmt.Println("Error decodificando archivo para rezise jpeg")
			return false
		}
		file.Close()
		// resize to width 1000 using Lanczos resampling
		// and preserve aspect ratio
		m := resize.Resize(1000, 0, img, resize.Lanczos3)

		CreateDirIfNotExist(URL_IMAGES + dbName)

		out, err := os.Create(URL_IMAGES + dbName + "/" + filename)
		if err != nil {
			fmt.Println("Error al escribiendo archivo en rezise")
			return false
		}
		defer out.Close()
		// write new image to file
		jpeg.Encode(out, m, nil)
		return true
	}
}

func DeleteFile(path string) (response bool) {
	// delete file
	error := os.Remove(path)
	if error != nil {
		fmt.Println(error.Error())
		return false
	}
	fmt.Println("==> done deleting file")
	return true
}

func CreateDirIfNotExist(dir string) {
	if _, err := os.Stat(dir); os.IsNotExist(err) {
		err = os.MkdirAll(dir, 0777)
		if err != nil {
			panic(err)
		}
	}
}
