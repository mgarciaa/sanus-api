package utils

import (
	"golang.org/x/crypto/bcrypt"
	"log"
)

func HashearPassword(password string) []byte {
	hash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		// TODO: Properly handle error
		log.Fatal(err)
	}
	return hash
}

func ComparePassword(hashFromDatabase []byte, password2 string) bool {
	if err := bcrypt.CompareHashAndPassword(hashFromDatabase, []byte(password2)); err != nil {
		// TODO: Properly handle error
		return false
	}
	return true
}
