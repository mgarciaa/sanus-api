package models

import (
	"gopkg.in/mgo.v2/bson"
	"time"
)

type Fechas struct {
	FechaInicio string `bson:"fechaInicio" json:"fechaInicio"`
	FechaFin string `bson:"fechaFin" json:"fechaFin"`
	FechaActual string `bson:"fechaActual" json:"fechaActual"`
}

type User struct {
	ID           bson.ObjectId `bson:"_id,omitempty" json:"_id,omitempty"`
	Nombre       string        `bson:"nombre,omitempty" json:"nombre,omitempty"`
	Usuario      string        `bson:"usuario" json:"usuario"`
	Password     string        `bson:"password" json:"password"`
	PasswordHash []byte        `bson:"passwordHash,omitempty" json:"passwordHash,omitempty"`
	Email        string        `bson:"email,omitempty" json:"email,omitempty"`
	Img          string        `bson:"img,omitempty" json:"img,omitempty"`
	Role         string        `bson:"role,omitempty" json:"role,omitempty"`
	Direccion    string        `bson:"direccion,omitempty" json:"direccion,omitempty"`
	Telefono     string        `bson:"telefono,omitempty" json:"telefono,omitempty"`
	Ciudad       string        `bson:"ciudad,omitempty" json:"ciudad,omitempty"`
	Create_date  time.Time     `bson:"create_date,omitempty" json:"create_date,omitempty"`
	Mod_date     time.Time     `bson:"mod_date,omitempty" json:"mod_date,omitempty"`
	Estado       int           `bson:"estado,omitempty" json:"estado,omitempty"`
	Token        string        `bson:"token,omitempty" json:"token,omitempty"`

}
type UserResponse struct {
	ID           bson.ObjectId `bson:"_id,omitempty" json:"_id,omitempty"`
	Nombre       string        `bson:"nombre,omitempty" json:"nombre,omitempty"`
	Usuario      string        `bson:"usuario" json:"usuario"`
	Email        string        `bson:"email,omitempty" json:"email,omitempty"`
}

