package models

import (
	"gopkg.in/mgo.v2/bson"
	"time"
)

type Producto struct {
	ID                bson.ObjectId `bson:"_id,omitempty" json:"_id,omitempty"`
	Categoria         string        `bson:"categoria" json:"categoria"`
	Nombre            string        `bson:"nombre" json:"nombre"`
	Precio            int           `bson:"precio" json:"precio"`
	Costo             int           `bson:"costo" json:"costo"`
	Descripcion       string        `bson:"descripcion" json:"descripcion"`
	Create_date       time.Time     `bson:"create_date" json:"create_date"`
	Estado            string        `bson:"estado" json:"estado"`
	Cantidad          int           `bson:"cantidad" json:"cantidad"`
	Percent_descuento int           `bson:"descuento" json:"descuento"`
	Destacado         bool          `bson:"destacado" json:"destacado"`
}

type ConteoProd struct {
	Nombre   string `bson:"nombre" json:"nombre"`
	Cantidad int    `bson:"cantidad" json:"cantidad"`
}

type Productos []Producto
