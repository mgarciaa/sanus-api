package models

type FinanzasRequest struct {
	Fecha_inicio string `bson:"fecha_inicio" json:"fecha_inicio"`
	Fecha_fin    string `bson:"fecha_fin" json:"fecha_fin"`
	UsuarioEmail string `bson:"usuarioEmail" json:"usuarioEmail"`
}

type FinanzasResponse struct {
	Pedidos                      `bson:"pedidos" json:"pedidos"`
	Cantidad_pedidos             int `bson:"cantidad_pedidos" json:"cantidad_pedidos"`
	Total_ventas                 int `bson:"total_ventas" json:"total_ventas"`
	Total_costos                 int `bson:"total_costos" json:"total_costos"`
	Total_gastos                 int `bson:"total_gastos" json:"total_gastos"`
	Total_costos_envio           int `bson:"total_costos_envio" json:"total_costos_envio"`
	Utilidad                     int `bson:"utilidad" json:"utilidad"`
	Utilidad_30                  int `bson:"utilidad_30" json:"utilidad_30"`
	Utilidad_50                  int `bson:"utilidad_50" json:"utilidad_50"`
	Utilidad_70                  int `bson:"utilidad_70" json:"utilidad_70"`
	Total_pedidos_perdidas       int `bson:"total_pedidos_perdidas" json:"total_pedidos_perdidas"`
	Total_costo_pedidos_perdidas int `bson:"total_costo_pedidos_perdidas" json:"total_costo_pedidos_perdidas"`
	Total_ganacia_sanus          int `bson:"total_ganacia_sanus" json:"total_ganacia_sanus"`
}
