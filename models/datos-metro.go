package models

type DatosMetro struct {
	Metro  string `bson:"metro,omitempty" json:"metro,omitempty"`
	Hora string `bson:"hora,omitempty" json:"hora,omitempty"`
}

type DatosMetros []DatosMetro
