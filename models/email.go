package models

type EmailData struct {
	Message string
	Firma   string
	Data    interface{}
}

type MailConfig struct {
	Email            EmailData
	To               string
	Cc               string
	CcName           string
	Subject          string
	TemplateUrl      string
	TemplateName     string
	TemplateImageUrl string
}
