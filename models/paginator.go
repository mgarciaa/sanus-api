package models

type Paginator struct {
	ItemsPerPage     int    `bson:"itemsPerPage" json:"itemsPerPage"`
	Pagina           int    `bson:"pagina" json:"pagina"`
	TotalRows        int    `bson:"totalRows,omitempty" json:"totalRows,omitempty"`
}
