package models

import (
	"gopkg.in/mgo.v2/bson"
	"time"
)

type MesGasto struct {
	ID              bson.ObjectId `bson:"_id,omitempty" json:"_id,omitempty"`
	Create_date     time.Time     `bson:"create_date" json:"create_date"`
	Fecha_string    string        `bson:"fecha_string" json:"fecha_string"`
	Mes             string        `bson:"mes" json:"mes"`
	Anio            string        `bson:"anio" json:"anio"`
	Fecha_mensual   time.Time     `bson:"fecha_mensual,omitempty" json:"fecha_mensual,omitempty"`
	GastosMensuales Gastos        `bson:"gastosMensuales" json:"gastosMensuales"`
}

type Gasto struct {
	Id              int    `bson:"id" json:"id"`
	Nombre          string `bson:"nombre" json:"nombre"`
	Precio_total    int    `bson:"precio_total" json:"precio_total"`
	Precio_unitario int    `bson:"precio_unitario,omitempty" json:"precio_unitario,omitempty"`
	Descripcion     string `bson:"descripcion,omitempty" json:"descripcion,omitempty"`
	Lugar_compra    string `bson:"lugar_compra,omitempty" json:"lugar_compra,omitempty"`
	Cantidad        int    `bson:"cantidad,omitempty" json:"cantidad,omitempty"`
	Fecha           string `bson:"fecha" json:"fecha"`
	Pagado          bool   `bson:"pagado" json:"pagado"`
	Pagar_a         string `bson:"pagar_a" json:"pagar_a"`
}

// Struct para request de fechas para metodo GetGastosByFechas
type GastoFechasRequest struct {
	Mes  string `bson:"mes" json:"mes"`
	Anio string `bson:"anio" json:"anio"`
}

type Gastos []Gasto
