package models

import (
	"gopkg.in/mgo.v2/bson"
	"time"
)

type PedidoT struct {
	Pedidos   `bson:"pedidos" json:"pedidos"`
	TotalRows int `bson:"totalRows" json:"totalRows"`
}

type OutputTotalPedidos struct {
	Nombre_prod string   `bson:"nombre_prod" json:"nombre_prod"`
	Cantidad_prod int `bson:"cantidad_prod" json:"cantidad_prod"`
}

type OutputDetalleDelivery struct {
	Nombre string   `bson:"nombre" json:"nombre"`
	Direccion string   `bson:"direccion" json:"direccion"`
	Telefono string   `bson:"telefono" json:"telefono"`
	Pagado bool   `bson:"pagado" json:"pagado"`
	Costo_envio int   `bson:"costo_envio" json:"costo_envio"`
}

type InputTotalPedidos struct {
	Fecha string   `bson:"fecha" json:"fecha"`
	Usuario string `bson:"usuario" json:"usuario"`
}

type PedidoResponseCosto struct {
	NombreProducto string `bson:"producto" json:"producto"`
	Precio         int    `bson:"precio" json:"precio"`
	Costo          int    `bson:"costo" json:"costo"`
	Ganancia       int    `bson:"ganancia" json:"ganancia"`
}

type Pedido struct {
	ID               bson.ObjectId `bson:"_id,omitempty" json:"_id,omitempty"`
	Numero           int           `bson:"numero,omitempty" json:"numero,omitempty"`
	Nombre           string        `bson:"nombre,omitempty" json:"nombre,omitempty"`
	Productos        `bson:"productos,omitempty" json:"productos,omitempty"`
	FechaCreacion    time.Time `bson:"fecha_creacion,omitempty" json:"fecha_creacion,omitempty"`
	CreatedAt        string    `bson:"created_at,omitempty" json:"created_at,omitempty"`
	FechaEntrega     string    `bson:"fecha_entrega,omitempty" json:"fecha_entrega,omitempty"`
	FechaEntrega2    time.Time    `bson:"fecha_entrega2,omitempty" json:"fecha_entrega2,omitempty"`
	TipoDespacho     string    `bson:"tipo_despacho,omitempty" json:"tipo_despacho,omitempty"`
	Pagado           bool      `bson:"pagado" json:"pagado"`
	DatosCliente     `bson:"datos_cliente,omitempty" json:"datos_cliente,omitempty"`
	DatosMetro       `bson:"datos_metro,omitempty" json:"datos_metro,omitempty"`
	FormaPago        string       `bson:"forma_pago,omitempty" json:"forma_pago,omitempty"`
	LugarContacto    string       `bson:"lugar_contacto,omitempty" json:"lugar_contacto,omitempty"`
	Observacion      string       `bson:"observacion,omitempty" json:"observacion,omitempty"`
	CostoEnvio       int          `bson:"costo_envio,omitempty" json:"costo_envio,omitempty"`
	Ciudad           string       `bson:"ciudad,omitempty" json:"ciudad,omitempty"`
	Estado           string       `bson:"estado,omitempty" json:"estado,omitempty"`
	UsuarioDesignado UserResponse `bson:"usuario_designado,omitempty" json:"usuario_designado,omitempty"`
	Descuento            int          `bson:"descuento" json:"descuento"`
	Total            int          `bson:"total,omitempty" json:"total,omitempty"`
}

func (this *Pedido) FormatDate() {
	this.CreatedAt = this.FechaCreacion.Format("02-01-2006 15:04")
}

type Pedidos []Pedido
