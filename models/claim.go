package models

import (
	"github.com/dgrijalva/jwt-go"
)

type Claim struct {
	Rol string `json:"rol"`
	Email string `json:"email"`
	Nombre string `json:"nombre"`
	Ciudad string `json:"ciudad"`
	jwt.StandardClaims
}
