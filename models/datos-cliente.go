package models

type DatosCliente struct {
	Telefono  string `bson:"telefono,omitempty" json:"telefono,omitempty"`
	Direccion string `bson:"direccion,omitempty" json:"direccion,omitempty"`
}

type DatosClientes []DatosCliente
