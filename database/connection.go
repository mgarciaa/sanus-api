package database

import (
	"github.com/joho/godotenv"
	"gitlab.com/skolldevs-backend/sanus-api/utils"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"log"
	"os"
)

var session *mgo.Session

const (
	DB_CONNECTION        = "DB_CONNECTION"
	FILE_NAME_CONNECTION = "application.env"
)

func createDBSession() {
	var err error
	_ = godotenv.Load(utils.GetUserWorkDir() + FILE_NAME_CONNECTION)
	session, err = mgo.Dial(os.Getenv(DB_CONNECTION))
	if err != nil {
		log.Fatal(err)
	}
}
func getSession() *mgo.Session {
	if session == nil {
		createDBSession()
	}
	return session
}

func CloseSession() {
	session.Close()
}

func GetCollection(collectionName string) *mgo.Collection {
	var collection = getSession().DB(DATABASE).C(collectionName)
	return collection
}

func CreateSecuence(collectionName string) (int, error) {

	seq := Secuence{}
	collection := GetCollection(SEQUENCES)

	collection.FindId("_id" + collectionName).One(&seq)

	var withErr error
	if seq.ID == "" {
		seq.ID = "_id" + collectionName
		seq.SecuenceValue = 1
		if err := collection.Insert(seq); err != nil {
			withErr = err
		}
	} else {
		document := bson.M{"_id": "_id" + collectionName}
		seq.SecuenceValue += 1
		change := bson.M{"$set": bson.M{"secuence_value": seq.SecuenceValue}}
		withErr = collection.Update(document, change)
	}
	return seq.SecuenceValue, withErr
}

type Secuence struct {
	ID            string `bson:"_id"`
	SecuenceValue int    `bson:"secuence_value"`
}
