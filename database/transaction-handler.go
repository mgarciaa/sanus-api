package database

import (
	"errors"
	"gopkg.in/mgo.v2/bson"
)

func RollbackHandler(database string, collectionName string, _id bson.ObjectId) error {

	collection := GetCollection(collectionName)

	if e := collection.RemoveId(_id); e != nil {
		return errors.New("No se pudo eliminar coleccion")
	}
	rollbackSecuence(database, collectionName)
	return nil
}

func rollbackSecuence(database string, collectionName string) {
	seq := Secuence{}
	collection := GetCollection(SEQUENCES)
	collection.FindId("_id" + collectionName).One(&seq)
	if seq.ID != "" {
		if seq.SecuenceValue == 0 {
			collection.RemoveId("_id" + collectionName)
		}
		document := bson.M{"_id": "_id" + collectionName}
		change := bson.M{"$set": bson.M{"secuence_value": seq.SecuenceValue - 1}}
		collection.Update(document, change)
	}
}
