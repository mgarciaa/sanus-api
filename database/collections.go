package database

const
(
	DATABASE        = "sanus"
	SEQUENCES       = "sequences"
	ESTADOS         = "estados"
	CATEGORIAS      = "categorias"
	USUARIOS        = "usuarios"
	INFORMACION_WEB = "informacionWeb"
	AGREGADOS       = "agregados"
	PEDIDOS         = "pedidos"
	PRODUCTOS       = "productos"
	GASTOS          = "gastos"
	PROMOCIONES     = "promociones"
)
