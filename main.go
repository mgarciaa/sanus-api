package main

import (
	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"gitlab.com/skolldevs-backend/sanus-api/routes"
	"log"
	"net/http"
)

func main() {

	r := mux.NewRouter()

	routes.AddRoutes(r)

	c := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowCredentials: true,
		AllowedHeaders:   []string{"Access-Control-Allow-Headers", "Accept", "Content-Type", "Content-Length", "Accept-Encoding", " X-CSRF-Token", "Authorization", "X-Requested-With"},
		AllowedMethods:   []string{"GET", "HEAD", "POST", "PUT", "OPTIONS", "DELETE"},
	})

	handler := c.Handler(r)

	if err := http.ListenAndServe(":9292", handler); err != nil {
		log.Fatal(err)
	}

}
